# Padding Oracle Attack

This repository contains a simple implementation of a CBC (Cipher Block Chaining) in Rust using AES128. It will be used to perform a _Padding oracle_ attack.

## CBC-RS

A simple implementation of a CBC (Cipher Block Chaining) in Rust using AES128.

### Padding (PKCS#7)

The padding adds the number of missing bytes $p$ to fill a full block (of 16 bytes by default) at the end of the message, repeated $p$ times. I.e. we have a message of length $n$ and blocks size of $b$, then $n + p = \left\lceil \frac{n}{b} \right \rceil \cdot b$, so we can get $p$ with $p = b - (n \mod b)$

### Build and run

```bash
$ cargo run --bin cbc-rs -- -h
padding_oracle 0.1.0

USAGE:
    cbc-rs [FLAGS] <message> --iv <iv> --key <key>

FLAGS:
    -d, --decrypt    Encrypt or decrypt
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --iv <iv>      128-bit IV (16 bytes) in hex
    -k, --key <key>    128-bit key (16 bytes) in hex

ARGS:
    <message>    Message to encrypt/decrypt
```

Cargo is the Rust build tool. `cargo run` will compile the code and execute it. To cipher a message, you need to provide a 128-bit key and a 128-bit IV (Initialization Vector) in hexadecimal. The message can be any string.

#### Encrypt

```bash
$ cargo run --bin cbc-rs -- -k 00112233445566778899aabbccddeeff -i 00112233445566778899aabbccddeeff "Hello world!"
fea23e996a24a66d5f57d348aa63a8ba
```

#### Decrypt

```bash
$ cargo run --bin cbc-rs -- -k 00112233445566778899aabbccddeeff -i 00112233445566778899aabbccddeeff -d fea23e996a24a66d5f57d348aa63a8ba
Hello world!
```

## Oracle

The oracle is a server that will decrypt a message and return an error if the padding is incorrect.

### Build and run

```bash
$ cargo run --bin oracle -- -h
padding_oracle 0.1.0

USAGE:
    oracle [FLAGS] [OPTIONS] <ciphertext>

FLAGS:
    -d, --daemon     Daemon mode
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -l, --listen <address>    Listen address

ARGS:
    <ciphertext>    Ciphertext to decrypt (hex)
```

### Daemon mode

```bash
$ cargo run --bin oracle -- -d -l 0.0.0.0:1234
```

This will start a server listening on port 1234. You can then send a ciphertext to decrypt.

#### Protocol

The server will first read 4 bytes (32 bits) representing the length of the ciphertext, then the ciphertext itself. It will then decrypt the ciphertext and return a byte representing the padding. If the padding is correct, it will return `0x00`, otherwise it will return `0x01`. It can return other values if an error occurs.

```plantuml
@startuml
Client -> Server: cipher length (4 bytes): 0x00000010
Client -> Server: ciphertext: 0x00112233445566778899aabbccddeeff
Server -> Client: padding status (1 byte): 0x00 (correct padding)
@enduml
```

## Padding oracle attack

The goal of the attack is to decrypt a ciphertext without knowing the key. The only thing we know is that the server will return an error if the padding is incorrect.

The attack is implemented in Python in the `python_scripts` directory. It will first send a ciphertext to the server, then it will try to decrypt it by changing the last byte of the ciphertext. If the padding is correct, it will try to decrypt the second to last byte, and so on.
