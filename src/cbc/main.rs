use cbc_rs::{cbc_decrypt, cbc_encrypt, pad, unpad};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    /// 128-bit key (16 bytes) in hex
    #[structopt(short, long)]
    key: String,

    /// 128-bit IV (16 bytes) in hex
    #[structopt(short, long)]
    iv: String,

    /// Encrypt or decrypt
    #[structopt(short, long)]
    decrypt: bool,

    /// Message to encrypt/decrypt (hex)
    #[structopt()]
    message: String,
}

fn main() {
    let opt = Opt::from_args();

    let key = hex::decode(&opt.key).unwrap();
    let iv = hex::decode(&opt.iv).unwrap();
    let message = opt.message.as_bytes();

    if opt.decrypt {
        match hex::decode(message) {
            Ok(ciphertext) => match unpad(&cbc_decrypt(&key, &iv, &ciphertext)) {
                Ok(m) => println!("{}", String::from_utf8(m.to_vec()).unwrap()),
                Err(e) => {
                    println!("{}", e);
                    std::process::exit(1);
                }
            },
            Err(e) => {
                println!("{}", e);
                std::process::exit(2);
            }
        }
    } else {
        let padded = pad(message);
        println!("{}", hex::encode(cbc_encrypt(&key, &iv, &padded)));
    }
}
