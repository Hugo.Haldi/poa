use std::{borrow::Cow, fmt};

use aes::{
    cipher::{generic_array::GenericArray, typenum::U16, BlockDecrypt, BlockEncrypt, KeyInit},
    Aes128,
};

use pyo3::{exceptions::PyValueError, prelude::*};

#[derive(Debug)]
pub struct PaddingError;

impl std::error::Error for PaddingError {}

impl fmt::Display for PaddingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Padding error")
    }
}

impl std::convert::From<PaddingError> for PyErr {
    fn from(err: PaddingError) -> Self {
        PyValueError::new_err(err.to_string())
    }
}

const BLOCK_SIZE: u8 = 16;

#[pyfunction]
pub fn pad(m: &[u8]) -> Cow<[u8]> {
    let padding_size = BLOCK_SIZE - u8::try_from(m.len() % usize::from(BLOCK_SIZE)).unwrap();
    let padding = vec![padding_size; padding_size.into()];
    [m, &padding].concat().into()
}

#[pyfunction]
pub fn unpad(m: &[u8]) -> Result<Cow<[u8]>, PaddingError> {
    let padding_size = *m.last().unwrap();
    if padding_size == 0 || padding_size > BLOCK_SIZE {
        return Err(PaddingError);
    }

    for i in m.len() - usize::from(padding_size)..m.len() - 1 {
        if m[i] != padding_size {
            return Err(PaddingError);
        }
    }

    Ok(m[..m.len() - usize::from(*m.last().unwrap())].into())
}

#[pyfunction]
pub fn cbc_encrypt<'a>(key: &[u8], iv: &[u8], m: &[u8]) -> Cow<'a, [u8]> {
    let mut tmp = u128::from_be_bytes(iv.try_into().unwrap());
    let mut ciphertext = Vec::<u8>::new();

    let cipher = Aes128::new(GenericArray::from_slice(key));

    for b in m.chunks(16) {
        let a = u128::from_be_bytes(b.try_into().unwrap());
        let mut cipherblock: GenericArray<_, U16> = GenericArray::from((a ^ tmp).to_be_bytes());
        cipher.encrypt_block(&mut cipherblock);
        tmp = u128::from_be_bytes(cipherblock.into());

        ciphertext.append(&mut Vec::from(cipherblock.as_mut_slice()));
    }

    return ciphertext.into();
}

#[pyfunction]
pub fn cbc_decrypt<'a>(key: &[u8], iv: &[u8], ciphertext: &[u8]) -> Cow<'a, [u8]> {
    let mut tmp = u128::from_be_bytes(iv.try_into().unwrap());
    let mut decipher = Vec::<u8>::new();

    let cipher = Aes128::new(GenericArray::from_slice(key));

    for b in ciphertext.chunks(16) {
        let mut cipherblock: GenericArray<_, U16> = GenericArray::clone_from_slice(b);
        cipher.decrypt_block(&mut cipherblock);

        let a = u128::from_be_bytes(cipherblock.try_into().unwrap());
        decipher.append(&mut Vec::from((a ^ tmp).to_be_bytes()));
        tmp = u128::from_be_bytes(b.try_into().unwrap());
    }

    return decipher.into();
}

#[pymodule]
fn cbc_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(cbc_encrypt, m)?)?;
    m.add_function(wrap_pyfunction!(cbc_decrypt, m)?)?;
    m.add_function(wrap_pyfunction!(pad, m)?)?;
    m.add_function(wrap_pyfunction!(unpad, m)?)?;
    Ok(())
}
