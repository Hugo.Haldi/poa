use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use cbc_rs::{cbc_decrypt, unpad};
use std::{
    io::{BufReader, Read},
    net::TcpListener,
};
use structopt::StructOpt;

const ORACLE_OK: u8 = 0;
const ORACLE_INVALID_PADDING: u8 = 1;
const ORACLE_INVALID_ENCODING: u8 = 2;
const ORACLE_INVALID_LENGTH: u8 = 3;

#[derive(StructOpt, Debug)]
struct Opt {
    /// Daemon mode
    #[structopt(short, long)]
    daemon: bool,

    /// Listen address
    #[structopt(short = "l", long = "--listen", requires("daemon"))]
    address: Option<String>,

    /// Ciphertext to decrypt (hex)
    #[structopt(required_unless = "daemon", conflicts_with = "daemon")]
    ciphertext: Option<String>,
}

fn main() {
    let key: u128 = 0x070a7535c7329ed50e6ff43f53904c1a;
    let iv: u128 = 0x5066deb26eec07fa9c59ac91cb7e9072;

    let opt: Opt = Opt::from_args();

    if opt.daemon {
        let address = opt.address.unwrap_or(String::from("0.0.0.0:1337"));
        let listener = TcpListener::bind(&address).unwrap();
        println!("Listening on {}", &address);

        for stream in listener.incoming() {
            let mut stream = stream.unwrap();

            println!("New connection from {}", &stream.peer_addr().unwrap());

            let mut buf_reader = BufReader::new(&mut stream);

            let len = buf_reader.read_u32::<BigEndian>().unwrap();

            let mut ciphertext = vec![0u8; len as usize];

            let response = if let Err(e) = buf_reader.read_exact(&mut ciphertext) {
                println!("{}", e);
                ORACLE_INVALID_LENGTH
            } else {
                match unpad(&cbc_decrypt(
                    &key.to_be_bytes(),
                    &iv.to_be_bytes(),
                    &ciphertext,
                )) {
                    Ok(_) => {
                        println!("Valid padding");
                        ORACLE_OK
                    }
                    Err(_) => {
                        println!("Invalid padding");
                        ORACLE_INVALID_PADDING
                    }
                }
            };

            stream.write_u8(response).unwrap();
        }
    } else {
        match hex::decode(&opt.ciphertext.unwrap()) {
            Ok(ciphertext) => match unpad(&cbc_decrypt(
                &key.to_be_bytes(),
                &iv.to_be_bytes(),
                &ciphertext,
            )) {
                Ok(_) => {
                    println!("Ok");
                    std::process::exit(ORACLE_OK as i32);
                }
                Err(e) => {
                    println!("{}", e);
                    std::process::exit(ORACLE_INVALID_PADDING as i32);
                }
            },
            Err(e) => {
                println!("{}", e);
                std::process::exit(ORACLE_INVALID_ENCODING as i32);
            }
        }
    }
}
