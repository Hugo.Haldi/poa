\documentclass{article}

% Language setting
% Replace `english' with e.g. `spanish' to change the document language
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[letterpaper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {./images/} }
\usepackage{svg}
\usepackage{wrapfig}
\usepackage{inputenc}
\usepackage{minted}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{subcaption}
\usepackage{plantuml}
%\usepackage[skip=10pt plus1pt, indent=40pt]{parskip}

\begin{document}
\begin{titlepage}
    \begin{center}
        \vspace*{4cm}

        \Huge
        Advanced Security

        \Huge
        \textbf{Padding Oracle Attack}

        \vspace{0.5cm}
        \Large
        \emph{ A comprehensive Padding Oracle Attack \\
            on a AES-CBC encrypted message }

        \vspace{0.5cm}
        \url{https://gitlab.unige.ch/Hugo.Haldi/poa}

        \vspace{1.5cm}

        \vfill
        \vspace{0.2cm}

        \includegraphics[width=0.6\textwidth]{logo.png}

        \Large
        H. Haldi, Computer Science Department  \\
        University of Geneva \\
        Geneva, Switzerland \\
        Spring 2023
    \end{center}
\end{titlepage}

\begin{abstract}
    AES is a widely used symmetric encryption algorithm that provides strong security and high efficiency for securing sensitive data. It operates on fixed-size data blocks using a secret key. The CBC mode of operation, commonly used with AES, introduces a chaining mechanism that enhances the algorithm's security by encrypting each block using the previously encrypted block.

    However, the CBC mode is vulnerable to a specific attack known as "padding oracle." This attack takes advantage of weaknesses in the handling of padding errors during data decryption. By manipulating the encrypted data and analyzing the responses of a padding oracle, an attacker can gradually reveal the original data, compromising system confidentiality.

    To counter padding oracle attacks when using CBC mode with AES, implementing appropriate safeguards is crucial. This may involve employing data integrity checks, deterministic padding schemes, or data authentication techniques to prevent exploitation of this vulnerability.

    Additionally, it is important to consider the risk of Power Consumption Analysis (PCA) attacks. PCA attacks exploit information leakage through power consumption patterns to deduce sensitive information like secret keys. To mitigate these risks, hardware-level protections and software-level techniques such as algorithmic masking or constant-time implementations can be employed.

    By implementing these safeguards, the security of CBC mode with AES can be enhanced, providing robust protection against padding oracle and power consumption analysis attacks.
\end{abstract}

\newpage

\tableofcontents

\newpage

\section{Introduction}

\subsection{AES}

AES (Advanced Encryption Standard) \cite{enwiki:aes} is a symmetric encryption algorithm used to secure sensitive data. It operates on data blocks of fixed size (usually 128 bits) and uses a secret key shared between the sender and the recipient.

The operation of AES is divided into four main stages: substitution, permutation, shuffling and expansion key.

In the first step, called "substitution", each byte in the input data block is replaced by another byte using a predefined substitution table called the "S-Box." This non-linear substitution introduces confusion into the data.

Then, in the "permutation" step, the bytes in the data block are rearranged so that their positions are mixed. This introduces scattering into the data.

After the permutation, the "shuffle" step is performed to further enhance security. In this step, the bytes in the block are combined together using specific mathematical operations, such as XOR, matrix multiplication, and bit shifting. These operations allow bytes to be mixed in a non-linear fashion and increase the complexity of the transformations.

Finally, the expansion key is used to generate additional subkeys for each round of encryption. These subkeys are derived from the original secret key and are used in the various stages of the AES algorithm.

AES uses a number of rounds of encryption, depending on the length of the key (128, 192 or 256 bits). Each round includes substitution, permutation, shuffling and key expansion steps. Data is iteratively processed through these rounds, enhancing the security and complexity of the algorithm.

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{AES-SubBytes}
        \caption{AES SubBytes "substitution" step}

        \includegraphics[width=\textwidth]{AES-ShiftRows}
        \caption{AES ShiftRows "permutation" step}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{AES-MixColumns}
        \caption{AES MixColumns "shuffling" step}

        \includegraphics[width=\textwidth]{AES-AddRoundKey}
        \caption{AES AddRoundKey "expansion key" step}
    \end{subfigure}
\end{figure}

In summary, AES is a symmetric encryption algorithm that operates on data blocks of fixed size. It uses substitution, permutation, shuffling and key expansion steps to encrypt data. The use of a number of encryption rounds and expansion keys depends on the length of the chosen key.

In the program I developed, I used the Rust programming language to implement the CBC mode of operation with AES. I used the RustCrypto aes crate\footnote{aes crate: \url{https://crates.io/crates/aes}} to implement the AES algorithm. We will now look at the CBC mode of operation in more detail.

\pagebreak

\subsection{CBC}

The CBC (Cipher Block Chaining) encryption mode \cite{enwiki:block-cipher-mode} is one of the operating modes commonly used with the AES (Advanced Encryption Standard) algorithm. It introduces a chaining mechanism between the data blocks, thus reinforcing the security of the algorithm.

The operation of the CBC mode can be described in four main steps: initialization, encryption, chaining and decryption.

1. Initialization:
CBC mode requires an initialization vector (IV) of size equal to the size of the data block used (e.g., 128 bits for AES). The IV is usually chosen randomly and must be different for each encrypted message. The IV is used to initialize the chaining process.

2. Encryption:
The first data block of the input message is combined with the IV using an XOR (exclusive OR) operation. The result of this operation is then encrypted using the AES algorithm with the secret key. The resulting ciphertext is the first encrypted data block.

For subsequent data blocks, the plaintext data block is also combined with the previous encrypted data block using an XOR operation. Then, the result of this operation is encrypted with the AES algorithm to obtain the next encrypted data block.

3. Chaining:
The encryption result of each data block becomes the encrypted data block for the next block. This chaining step ensures that minor changes in the input data propagate and affect all subsequent encrypted blocks. This increases the security of the encryption by making it more difficult to manipulate the encrypted data without knowledge of the secret key.

4. Decryption:
The decryption process follows essentially the same steps as encryption, but in reverse order. The encrypted data block is decrypted using the AES algorithm with the secret key to obtain the clear data block. Then, this clear data block is combined with the previous encrypted data block using the XOR operation to recover the previous clear data block.

The last clear data block is obtained by performing a final XOR operation with the IV. Thus, the final result is the original plaintext message.

\begin{figure}[H]
    \includegraphics[width=\textwidth]{CBC_encryption}
\end{figure}

\begin{figure}[H]
    \includegraphics[width=\textwidth]{CBC_decryption}
\end{figure}

The CBC mode provides increased security over simple independent block ciphering. Block chaining and the use of the IV ensure that even small changes in the input data result in large changes in the ciphertext. This makes it more difficult for an attacker to guess, manipulate or recover the original data without knowledge of the secret key.

\subsubsection{Padding}

This implementation of CBC mode uses PKCS7 padding.

PKCS7 (Public Key Cryptography Standards \#7) padding is a padding mechanism used to complete fixed-size data blocks in block ciphers. Its purpose is to ensure that the data to be encrypted reaches the size required by the encryption algorithm, even if it is not a full multiple of that size.

PKCS7 padding consists of adding extra bytes to the end of the data in order to extend it to the desired size. Each added byte has the same value, which represents the number of padding bytes added. For example, if a block of data needs to be expanded by 8 bytes, then 8 bytes of value 0x08 will be added. If a block must be extended by 4 bytes, then 4 bytes of value 0x04 will be added, and so on.

For padding removal, just check the value of the last byte. This value indicates how many bytes of padding have been added at the end. The padding bytes are then removed, leaving the original data without the added padding bytes.

PKCS7 padding is widely used in symmetric encryption systems such as Advanced Encryption Standard (AES) to ensure that the input data is of an appropriate size for block ciphering. It is simple to implement and provides data integrity by ensuring that blocks are filled in a consistent and predictable manner.

It should be noted that when decrypting, it is important to check if the padding is correct. If the padding is incorrect (e.g., the padding byte values are not consistent), this may indicate data corruption or a decryption error. In such cases, an error may be generated or the decrypted data may be considered invalid.

\subsubsection{CBC implmentation}

I implemented the CBC encryption mode with the Rust language and the AES128 algorithm. This language is interesting, especially in system programming and in security, thanks to its "borrow checker" which allows to ensure the validity of the data read in memory at any time, as well as the maintenance of the lifetime of a variable. This prevents errors such as buffer overflows or memory leaks.

The function \texttt{cbc\_encrypt} located in the file \texttt{src/lib.rs} takes as parameters the key (an \textit{slice} of 16 bytes totalling 128 bits), the IV (a 128-bit number) and the message to be encrypted (an \textit{slice} of bytes). The function \texttt{cbc\_decrypt} is similar.

The function \texttt{pad} adds a PKCS7 padding to the message, while the function \texttt{unpad} checks if the padding of the message is correct and returns the message \textit{unpadded} if it is, and an error otherwise.

\begin{figure}[H]
    \begin{minted}[breaklines,linenos]{rust}
#[pyfunction]
pub fn cbc_encrypt<'a>(key: &[u8], iv: &[u8], m: &[u8]) -> Cow<'a, [u8]> {
    let mut tmp = u128::from_be_bytes(iv.try_into().unwrap());
    let mut ciphertext = Vec::<u8>::new();

    let cipher = Aes128::new(GenericArray::from_slice(key));

    for b in m.chunks(16) {
        let a = u128::from_be_bytes(b.try_into().unwrap());
        let mut cipherblock: GenericArray<_, U16> = GenericArray::from((a ^ tmp).to_be_bytes());
        cipher.encrypt_block(&mut cipherblock);
        tmp = u128::from_be_bytes(cipherblock.into());

        ciphertext.append(&mut Vec::from(cipherblock.as_mut_slice()));
    }

    return ciphertext.into();
}
\end{minted}
    \caption{CBC encryption function in Rust}
\end{figure}

\begin{figure}[H]
    \begin{minted}[breaklines,linenos]{rust}
#[pyfunction]
pub fn unpad(m: &[u8]) -> Result<Cow<[u8]>, PaddingError> {
    let padding_size = *m.last().unwrap();
    if padding_size == 0 || padding_size > BLOCK_SIZE {
        return Err(PaddingError);
    }

    for i in m.len() - usize::from(padding_size)..m.len() - 1 {
        if m[i] != padding_size {
            return Err(PaddingError);
        }
    }

    Ok(m[..m.len() - usize::from(*m.last().unwrap())].into())
}
\end{minted}
    \caption{Padding removal function in Rust}
\end{figure}

The unpad function returns an error if the padding is incorrect. This will be used later for the Padding Oracle Attack.

All these functions can be called from Python with the module \texttt{pyo3} :

\begin{minted}[breaklines]{python}
from cbc_rs import cbc_encrypt, cbc_decrypt, pad, unpad

padded = pad(message)
print("Padded:", padded.hex())
ciphertext = cbc_encrypt(key, IV, padded)
print("Ciphertext:", ciphertext.hex())
deciphered = cbc_decrypt(key, IV, ciphertext)
print("Deciphered:", deciphered)
print("Unpadded:", unpad(deciphered))
\end{minted}

The above code displays the following result:

\begin{minted}[breaklines]{text}
Padded: 59454c4c4f57205355424d4152494e4510101010101010101010101010101010
Ciphertext: 18f33761839c80c8532081c2c1e29c407578867b83c84783dd0522e1ddc61935
Deciphered: b'YELLOW SUBMARINE\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10'
Unpadded: b'YELLOW SUBMARINE'
\end{minted}

This demonstrates the operation of CBC encryption using the AES128 algorithm with the Rust language and the interoperability with Python thanks to the \texttt{pyo3} module. The message is correctly padded, encrypted, decrypted and padding-free, confirming the correctness of the CBC mode implementation.

\section{Padding Oracle Attack}

\subsection{Overview}

The padding oracle attack \cite{enwiki:padding-oracle-attack} is a cryptographic attack that exploits a vulnerability in the padding verification used in some block ciphers (e.g. CBC). This attack allows an attacker to recover the plaintext message from the cipher text by exploiting the system's responses to the padding check.

The operation of the oracle padding attack relies on the fact that the padding used must be verified and removed during decryption. If the padding is incorrect, an error is usually reported by the system. By exploiting this information, an attacker can gradually recover the message in clear text, byte by byte.

Here are the general steps of the oracle padding attack:

1. Observation of the system's behavior: The attacker sends several encrypted messages to the system and observes the responses obtained during the padding verification. Specifically, the attacker tries to determine whether the padding is correct or not without knowing the secret key.

2. Binary search attack: The attacker starts by guessing the value of a byte in the last encryption block (the block that is being decrypted). Using a binary search process, he sends modified encrypted messages to the system to test different possible values for that byte. For each attempt, he observes the system's response.

3. Padding verification: When the system verifies the padding after decryption, there can be three possible cases:
- If the padding is correct, no error is reported. The attacker has therefore guessed the correct value for the byte.
- If the padding is incorrect but a specific error is returned (for example, an invalid padding error), this indicates that the guessed value is incorrect.
- If another error is returned (e.g., a decryption error), this may indicate an error in the data sent by the attacker and does not provide information about the guessed value.

4. Repeat attack: The attacker repeats steps 2 and 3 to guess each byte of the last encryption block, using the binary search process. By guessing the bytes one by one, the attacker can gradually recover the plaintext message.

5. Extending the attack: Once the attacker has recovered the last encryption block, he can continue to attack the previous blocks by moving up the chain. Using similar techniques, he can recover the bytes from each encryption block up to the first block.

\subsection{Technical details}

This section is based on the work of \textit{flast101} \cite{flast101:poracle}.

\subsubsection{CBC vulnerability}

Let's suppose we have a padded message of 4 blocks $P_0$ to $P_3$ and the corresponding ciphertext $C_0$ to $C_3$. We know by CBC mode that $C_0 = E_K(P_0 \oplus IV)$ and $C_i = E_K(P_i \oplus C_{i-1})$ for $i \in [2, 4]$. We also know that $P_4$ is padded with $n$ bytes of value $n$.

This gives us these two equations for crypting and decrypting:

\begin{equation}
    C_i = E_K(P_i \oplus C_{i-1})
\end{equation}

\begin{equation}
    P_i = D_K(C_i) \oplus C_{i-1}
\end{equation}

Now let's define a random block $X$ that will be modified to get correct padding. Let's encrypt $X + C_3$ ($X$ concatenated to $C_3$) and call the result $P_0' + P_1'$. Using the equations above, we can write the following:

$$C_3 = E_K(P_3 \oplus C_2)$$
$$P_1' = D_K(C_3) \oplus X$$

Now let's replace $C_3$ by $E_K(P_3 \oplus C_2)$ in the second equation:

\begin{equation*}
    \begin{split}
        P_1' & = D_K(E_K(P_3 \oplus C_2)) \oplus X \\
        & = P_3 \oplus C_2 \oplus X \\
    \end{split}
\end{equation*}

And since the XOR operation is commutative and associative, we can write:

\begin{equation*}
    P_3 = P_1' \oplus C_2 \oplus X
\end{equation*}

In this situation, we have two known values: $X$ and $C_2$, and two unknown values: $P_3$ and $P_1'$. However we will be able to determine $P_1'$ using the padding mechanism.

The decryption of an encrypted text must be a plaintext with a valid padding, i.e. the last $n$ bytes of the plaintext must be equal to $n$. We can make X vary to find a value that will give us a valid padding. If we find a value $X$ that gives us a valid padding, we will be able to determine $P_1'$ and therefore $P_3$.

\subsubsection{Attack}

\paragraph{Step 1: Last byte}

Let's first focus on the last byte of $P_3$. In this case, we suppose that $P_1'$ is padded with only one byte of value 0x01. We will therefore try to find the value of $X$ that will give us a valid padding for the ciphertext $X + C_3$.

Once we found a such $X$, we can determine the last byte of $P_3$ by computing $P_3[15] = P_1'[15] \oplus C_2[15] \oplus X[15] = \mathrm{0x01} \oplus C_2[15] \oplus X[15]$, where $B[n-1]$ is the $n$th byte of the bloc $B$.

\paragraph{Step 2: The next bytes}

Let's have a look at the previous byte of $P_3$, $P_3[14]$. We suppose now that $P_1'$ is padded with two bytes of value 0x02, i.e. $P_1'[15] = P_1'[14] = \mathrm{0x02}$. We now have the following:

$$X[15] = P_1'[15] \oplus C_2[15] \oplus P_3[15] = \mathrm{0x00} \oplus C_2[15] \oplus P_3[15]$$

where $P_3[15]$ has been found in the previous step and $C_2[15]$ is known, and:

$$P_3[14] = P_1'[14] \oplus C_2[14] \oplus X[14] = \mathrm{0x02} \oplus C_2[14] \oplus X[14]$$

where $C_2[14]$ is known and $X[14]$ can be found by bruteforce until the Oracle say that the deciphering of $X + C_2$ has a valid padding.

We can repeat this process until we find the first byte of $P_3$. Once the whole block has been found, we can repeat the process for the previous block until we find the whole plaintext.

However, when we reach the first block $P_0$, we can't use the previous block because it doesn't exist. Actually, in the formula $P_i = D_K(C_i) \oplus C_{i-1}$, $C_{i-1}$ doesn't exist for $i = 0$. We can use the IV instead of $C_{i-1}$, but we don't know the IV. So either we know the IV, in which case it can be seen as the previous ciphered block, or we don't know it, in which case we can try usual values such as 0x00, 0xFF, \dots

\paragraph{General formula}

Let's define some constants:

\begin{itemize}
    \item $M$ is the message length
    \item $B$ is the block size
    \item $N = M / B$ is the number of blocks
\end{itemize}

Now initialize $P$ as an empty array of bytes of size $M$. We will fill it with the plaintext.

\begin{equation*}
    \begin{split}
        P_{N-1}[B-1] & = P_1'[B-1] \oplus C_{N-2}[B-1] \oplus X[B-1] \\
        & = \mathrm{0x01} \oplus C_{N-2}[B-1] \oplus X[B-1]
    \end{split}
\end{equation*}

where $X[B-1]$ is found by bruteforce until the padding of ciphertext $X[B-1] + C_{N-2}$ is valid.

We can now iterate on $i = 0..B-1$ and $j = 0..N-1$ to find the whole plaintext:

\begin{equation*}
    \begin{split}
        X[i+1] & = P_1'[i+1] \oplus C_{j-1}[i+1] \oplus P_j[i+1] \\
        & = padding \oplus C_{j-1}[i+1] \oplus P_j[i+1]
    \end{split}
\end{equation*}

where $C_{j-1}[i+1]$ and $P_j$[i+1] are known,

\begin{equation*}
    \begin{split}
        P_j[i] & = P_1'[i] \oplus C_{j-1}[i] \oplus X[i] \\
        & = padding \oplus C_{j-1}[i] \oplus X[i]
    \end{split}
\end{equation*}

where $X[i]$ is the byte found by bruteforce until the padding of ciphertext $X[i] + C_{j-1}$ is valid.

\subsection{Implementation}

The first objective of this project was to analyse power consumption traces of a microcontroller during the execution of the AES128 algorithm in CBC mode. However this would have taken too much time and would have been too complex to implement. I therefore decided to simulate the microcontroller with a server that will anser wether the padding is correct or not.

The server is written in Rust and follows this protocol:

\begin{figure}[H]
    \centering
    \input{Oracle Protocol.tex}
    \caption{Oracle Protocol}
\end{figure}

The status byte is 0x00 if the padding is correct, 0x01 otherwise. It can also be different if an error occured. Now we can use a Python script to perform the attack. The script will query the server to know if the padding is correct or not.

The exploit is in the file \texttt{python\_scripts/poracle\_exploit.py} and an example is in the Jupyter Notebook. The Oracle server is in the file \texttt{src/oracle/main.rs}. The oracle has a key and an IV hardcoded in the source code. The key is \texttt{0x070a7535c7329ed50e6ff43f53904c1a} and the IV is \texttt{0x5066deb26eec07fa9c59ac91cb7e9072}. The Oracle server can be start with the command \texttt{cargo run --bin oracle -- -d}. It listens on port 1337 by default, however it can be changed with the \texttt{-l} option, e.g. \texttt{cargo run --bin oracle -{}- -d -l 127.0.0.1:1234} to listen on port 1234 of the localhost.

\subsubsection{Running the attack}

We can define the oracle function as follows:

\begin{minted}[breaklines]{python3}
def oracle(encrypted: bytes, oracle_host="127.0.0.1", oracle_port=1337):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((oracle_host, oracle_port))
        s.sendall(len(encrypted).to_bytes(4, byteorder='big'))
        s.sendall(encrypted)
        data = s.recv(1)
        return data == b'\x00'
\end{minted}

I made an example in the Jupyter Notebook to illustrate how the attack works. I encrypted a message using the oracle key and IV and then I tried to decrypt it using the padding oracle attack.

The ciphertext is (in hexadecimal):

\begin{minted}[breaklines]{text}
cb7b5d1e30b14dd3b9b06a3eff3c0c1c8b80454cbb87280538b0c2d000bbd28316
ac69f32525ba57af49a5a5c72477463f8f09209e2311eebd0c1b543def90d62e77
6e1976ba8717a88f5aa39fc77c56b8ab9d7b8c74b5fadedd0cf9adcd85dbab02bf
08049bdc55e28462ba7fef5f4d
\end{minted}

If we don't know the IV, we cannot decrypt the first block of the message. We can however decrypt the following blocks.

The decrypted message is:

\textit{[...]you wrote more than two weeks ago is like looking at code you are seeing for the first time.}

Here we cannot know the beginning of the message since we don't know the IV.

Now let's try to decrypt the message with the known IV:

IV = \texttt{0x5066deb26eec07fa9c59ac91cb7e9072}

The decrypted message is:

\textit{Looking at code you wrote more than two weeks ago is like looking at code you are seeing for the first time.}

\subsection{Conclusion}

The padding oracle attack is a complex attack that requires many requests to the target system. However, it can be effective in certain scenarios where the attacker has access to the system and can observe the system's responses during the padding check. To protect against this attack, it is important to implement robust defense mechanisms, such as removing specific padding to avoid providing sensitive information to a potential attacker. In this specific case, the oracle should not return a specific error code when the padding is incorrect, but rather a generic error code, and implement a mechanism to detect brute force attacks (e.g. by limiting the number of requests per IP address).

\section{Power Consumption Analysis (PCA)}

As mentioned before, the first attempt of this project was to use PCA to analyse the power consumption traces of a microcontroller during the execution of the AES128 algorithm in CBC mode. However, since it would have taken too much time, I decided to simulate the microcontroller with a server that will answer wether the padding is correct or not. Few concepts explained in this section comes from the book \textit{The Hardware Hacking Handbook} \cite{van2021hardware}

\subsection{Definition}

Power analysis is a side-channel attack that consists in analysing the power consumption of a device during its execution. This attack is based on the fact that the power consumption of a device depends on the operations performed by the device. By analysing the power consumption, it is possible to deduce information about the operations performed by the device.

\subsection{Example: Password verification}

\subsubsection{How it works}

A simple example is the authentication of a user on a device using a password. When the user enters his password, the device performs a series of operations to verify the password. The power consumption of the device will depend on the operations performed by the device. By analysing the power consumption, it is possible to deduce information about the password. For example, if the first character of the password is incorrect, the power consumption will drop quickly because the device will stop the verification process. If the first character is correct, the power consumption will remain high because the device will continue the verification process. We can do the same for each character of the password and thus deduce the password in a linear time.

\subsubsection{Theorical time complexity}

Suppose we have a password of length $n$ and a set of $m$ possible characters. At the first step, we have $m$ possible characters for the first character of the password. Then, once the first character is found, we have $n-1$ characters to find with $m$ possible characters for each character. We can therefore deduce the time complexity of the attack:

$$\alpha \cdot n \cdot m$$

where $\alpha$ is a constant that depends on how much time it takes to produce a power consumption trace and to analyse it.

\subsection{How to exploit Padding Oracle Attack with PCA}

The goal is to exploit PCA to get some information on the padding. The setup is the following:

\begin{itemize}
    \item The Oracle runs on a microcontroller that will be the target of the attack
    \item The Oracle ask the user for a message to decrypt but don't give the answer to the user
    \item The attacker has access to the power consumption traces of the microcontroller
    \item The attacker has access to some ciphertext encrypted by the Oracle
    \item The attacker give the ciphertext to the Oracle and ask him to decrypt it
    \item The Oracle will decrypt the ciphertext and check if the padding is correct, and perform some operations depending on the result
    \item The attacker should be able to deduce information on the padding by analysing the power consumption traces
    \item Use the information on the padding to perform the padding oracle attack
\end{itemize}

In this setup, each step of the attack is the modification of a single byte of the ciphertext. The attacker have to analyse traces at each step to determine if the padding is correct or not. The number of traces needed to correctly determine the padding status depends on the precision of the analysis and the time difference between the two cases. The more we have noise in the traces, the more traces we need to analyse to determine the padding status.

This setup seems not so hard to implement, unfortunately it was a bit too much for this project. But I think it is a good idea to exploit PCA to perform the padding oracle attack.

\subsection{How to protect against PCA}

There are various measures to safeguard against PCA. One of them is to ensure that the operations executed by the device are time-invariant. This will result in constant power consumption, making it challenging to extract information on the operations performed. Nonetheless, this approach may not be feasible in all cases. Another option is to introduce noise into the power consumption, which makes analysis more difficult. Although not foolproof, it still makes the attack more difficult to execute.

Use of PCA-resistant algorithms: Some encryption algorithms are inherently more resistant to PCA attacks. For example, encryption modes that minimize variations in power consumption by performing homogeneous operations on all bits in the block may be preferred.

\section{Conclusion}

In conclusion, the Padding Oracle Attack is a specific cryptographic attack that targets the padding verification mechanism used in block ciphers, such as the CBC mode of operation with AES. This attack takes advantage of the padding error responses from a server or system to gradually reveal the original plaintext message, compromising the confidentiality of the data.

The CBC mode of operation with AES is widely used for its strong security and efficiency in protecting sensitive data. However, the padding oracle vulnerability poses a significant risk to the security of encrypted messages. By manipulating the encrypted data and analyzing the padding error responses, an attacker can deduce the content of the message without knowledge of the secret key.

To mitigate the risk of padding oracle attacks, several safeguards can be implemented. These include using data integrity checks, deterministic padding schemes, or data authentication techniques to prevent the exploitation of the vulnerability. Additionally, considering the risk of Power Consumption Analysis (PCA) attacks is essential, as these attacks exploit information leakage through power consumption patterns to deduce sensitive information like secret keys. Hardware-level protections and software-level techniques such as algorithmic masking or constant-time implementations can help mitigate the risk of PCA attacks.

Implementing these safeguards can enhance the security of the CBC mode with AES, providing robust protection against padding oracle and power consumption analysis attacks. It is crucial to stay informed about the latest security measures and updates in cryptographic algorithms to ensure the continued resilience of data protection systems.

The implementation of the CBC mode of operation with AES using the Rust programming language showcased in this document highlights the importance of secure coding practices and the interoperability between different programming languages. The integration of Rust and Python demonstrates the flexibility and practicality of implementing cryptographic algorithms and conducting security assessments.

Overall, understanding the vulnerabilities and countermeasures associated with the Padding Oracle Attack is crucial for ensuring the security of encrypted data. By implementing appropriate safeguards and staying vigilant against emerging threats, organizations and individuals can maintain the confidentiality and integrity of their sensitive information.

\bibliographystyle{plain} % We choose the "plain" reference style
\bibliography{refs} % Entries are in the refs.bib file

\end{document}