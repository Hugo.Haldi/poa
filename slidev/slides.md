---
theme: academic
layout: cover
class: text-white text-center
coverBackgroundUrl: https://source.unsplash.com/collection/94734566/1920x1080
coverAuthor: Hugo Haldi
coverAuthorUrl: https://gitlab.unige.ch/Hugo.Haldi
highlighter: shiki
lineNumbers: false
info: |
  # Padding Oracle Attack on AES-CBC
  ## Advanced Security Project

  [GitLab](https://gitlab.unige.ch/Hugo.Haldi/poa)
drawings:
  persist: false
transition: slide-left
css: unocss
title: Padding Oracle Attack on AES-CBC
---

# Padding Oracle Attack on AES-CBC

## Advanced Security Project

[gitlab.unige.ch/Hugo.Haldi/poa](https://gitlab.unige.ch/Hugo.Haldi/poa)

---

## Introduction

Attack rely on CBC operation mode

CBC can use any block cipher, in this case AES

Padding oracle attack is a side-channel attack

---

## CBC

Block cipher mode of operation

Encrypts each block of plaintext separately

Each block of ciphertext depends on all blocks of plaintext

$c_i = E_k(p_i \oplus c_{i-1})$

![CBC encryption](/images/CBC_encryption.svg)

---

## CBC

Decryption

$$
\begin{aligned}
  D_k(c_i) = D_k(E_k(p_i \oplus c_{i-1})) & \iff D_k(c_i) = p_i \oplus c_{i-1} \\
  & \iff p_i = D_k(c_i) \oplus c_{i-1}
\end{aligned}
$$

![CBC decryption](/images/CBC_decryption.svg)

---

## Padding (PKCS#7)

Padding is used to fill the last block of plaintext

E.g. if a block is 8 bytes long and the plaintext is 5 bytes long, 3 bytes of padding are added

Padded bytes are all equal to the number of bytes added

### Example

![Padding](/images/pkcs7.svg)

If the plaintext is already a multiple of the block size, a new block is added

---

## Padding Oracle

System that can tell if a ciphertext has valid padding

I.e. we give it a ciphertext and it tells us if the padding is valid

- System receives a ciphertext
- System decrypts the ciphertext
- System checks if the padding is valid
- System returns whether the padding is valid

Oracle can be:

- A web server
- or a hardware device

---

## Padding Oracle using PCA

First attempt: analyze power consumption of a device to determine if the padding is valid

The device should consume more power if the padding is valid because it will perform more operations

Method:

- Send a bad padding ciphertext to the device
- Measure the average power consumption
- Send a ciphertext we want to decrypt
- Measure traces of power consumption
- Compare the traces to the average

Not enough time to implement this method

---

## Padding Oracle using TCP server

Second attempt: use a TCP server as an oracle

- Server receives a ciphertext
- Server decrypts the ciphertext
- Server checks if the padding is valid
- Server returns whether the padding is valid

Server owns the secret key and initialization vector (IV) used for encryption

---

## Padding Oracle using TCP server

```mermaid
sequenceDiagram
  Client->>Server: cipher length (4 bytes): 0x00000010
  Client->>Server: ciphertext: 0x00112233445566778899aabbccddeeff
  Server->>Server: decrypt ciphertext
  Server->>Server: check padding
  Server->>Client: padding status (1 byte): 0x00 (correct padding)
```

---
layout: two-cols
---

## Padding Oracle Attack

Once we have a padding oracle, we can perform our attack

Suppose the following setup:

- Plaintext: $p_1, p_2, p_3, p_4$
- Ciphertext: $c_1, c_2, c_3, c_4$

We will start to decrypt $p_3$.

By definition of CBC, we know that:

$$c_3 = E_k(p_3 \oplus c_2)$$

::right::

![Four blocs](/images/four_blocks.png)

---
layout: two-cols
---

::right::

![Two blocs](/images/two_blocks.svg)

::default::

We know that

$$p_i = D_k(c_i) \oplus c_{i-1}$$

Let's generate some random bloc $x$ and concatenate $x$ and $c_3$.

Using our diagram, we can write the following:

$$p'_1 = D_k(c_3) \oplus x$$

However, $c_3 = E_k(p_3 \oplus c_2)$, so we can write

$$
p_1' = p_3 \oplus c_2 \oplus x \\
\iff \\
p_3 = p_1' \oplus c_2 \oplus x
$$

---
layout: two-cols
---

::right::

![Two blocs](/images/two_blocks.svg)

::default::

$$p_3 = p_1' \oplus c_2 \oplus x$$

- We know: $x$ and $c_2$
- $p_3$: last plaintext block we want to decrypt
- $p_1'$: plaintext comming from the concatenation of $x$ and $c_3$

However:

- The decryption of an encrypted text must be a valid plaintext, i.e. it must have a valid padding (ends with 0x01, 0x02 0x02, etc.), so we know the end of $p_1'$
- We can make $x$ vary until we find a valid padding for $D_k(x + c_3)$ ($x$ concatenated with $c_3$)

---
layout: two-cols
---

::right::

![Two blocs](/images/two_blocks.svg)

::default::

### Get the last byte

$$p_3 = p_1' \oplus c_2 \oplus x$$

How to get the last byte of $p_3$ ?

We want to find $x$ such that $p_1'$ ends with 0x01

1. Generate a random $x$
2. Ask the oracle if $x + c_3$ has a valid padding
3. Increment the last byte of $x$ and repeat step 2
4. Once we find $x$ such that $D_k(x + c_3)$ has a valid padding, we can compute the last byte of $p_3$:

$$
\begin{aligned}
  p_3[15] & = p_1'[15] \oplus c_2[15] \oplus x[15] \\
  & = \mathrm{0x01} \oplus c_2[15] \oplus x[15]
\end{aligned}
$$

---

### Get the next bytes

Let's start with the second to last byte of $p_3$

Now, we want to find $x$ such that $p_1'$ ends with 0x02 0x02

First, compute $x[15]$:

$$
\begin{aligned}
  x[15] & = p_1'[15] \oplus c_2[15] \oplus p_3[15] \\
  & = \mathrm{0x02} \oplus c_2[15] \oplus p_3[15]
\end{aligned}
$$

where $p_3[15]$ is the last byte we computed in the previous step

---

### Get the next bytes (cont.)

Then, similarly to the previous step, we can compute $p_3[14]$:

1. Ask the oracle if $x + c_3$ has a valid padding
2. Increment the second to last byte of $x$ and repeat step 1
3. Once we find $x$ such that $D_k(x + c_3)$ has a valid padding, we can compute the second to last byte of $p_3$:

$$
\begin{aligned}
  p_3[14] & = p_1'[14] \oplus c_2[14] \oplus x[14] \\
  & = \mathrm{0x02} \oplus c_2[14] \oplus x[14]
\end{aligned}
$$

---

### Get the next bytes (cont.)

We can repeat this process until we decrypt the whole block, then we can move on to the next block and repeat the whole process

At step $i=1..B$, we have:

$$
\begin{aligned}
  p_3[B - i] & = (p_1')^i[B - i] \oplus c_2[B - i] \oplus x^i[B - i] \\
  & = i \oplus c_2[B - i] \oplus x^i[B - i]
\end{aligned}
$$

where the byte $x^i[B - i]$ is found by incrementing until we find a valid padding

And we can prepare the next step ($i+1$) by computing the $i+1$ last bytes of $x^{i+1}$:

$$
\begin{aligned}
  x^{i+1}[B - j] & = (i+1) \oplus c_2[B - j] \oplus p_3[B - j] \\
\end{aligned}
$$

for $j=1..i$

---

## Demo !

---

## References

[flast101's blog](https://flast101.github.io/padding-oracle-attack-explained/)
